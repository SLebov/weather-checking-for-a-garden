#include <Display.h>
#include <DHT11.h>

const int YELLOW_LIGHT = 7;
const int BLUE_LIGHT = 6;
const int GREEN_LIGHT = 5;
const int RED_LIGHT = 4;

const int BUTTON1 = 8;
const int BUTTON2 = 9;

const int BUZZER = 3;
const int TONE_C4 = 261;
const int QUARTERNOTE = 1000;
const int maxTemp = 27;

const int minTemp = 16;
//Declare Temperature Sensor
const int TEMP = A1;
const int topshades = 100;

const int LONG_PAUSE = 250;
const int SHORT_PAUSE = 75;

const int TEMP_R25 = 10000;               // the resistance of the NTC at 25'C is 10k ohm
const int TEMP_MATERIAL_CONSTANT = 3950;  // value provided by manufacturer
const int PIN_POTMETER = 14;
const int MAX_ANGLE = 20;
const int Interval = 1000;
unsigned long prevTime;
unsigned long currentTime;

int LastKnobState;
const int minhumidity = 81;
//your limit humidity.
const int PIN_LDR = 16;

//Debouce of the code.

int Mode_buttonState = HIGH;             // the current reading of TempButton
int Mode_lastButtonState = HIGH;         // the previous reading of TempButton
int Alarm_buttonState = HIGH;            // the current reading of BrightButton
int Alarm_lastButtonState = HIGH;        // the previous reading of BrightButton

unsigned long Mode_lastDebounceTime = 0;    // the last time the TempButton was toggled
unsigned long debounceDelay = 50;
unsigned long Alarm_lastDebounceTime = 0;  // the last time the BrightButton was toggled
unsigned long debounceDelay1 = 50;

int Mode = 0;
int ModeState = 0;

// should show the debouce inside the code.

void setup() {
  // put your setup code here, to run once:
  pinMode (RED_LIGHT, OUTPUT);
  pinMode (BLUE_LIGHT, OUTPUT); 
  pinMode (GREEN_LIGHT, OUTPUT); 
  pinMode (YELLOW_LIGHT, OUTPUT); 
  
  pinMode (BUTTON1, INPUT_PULLUP);
  pinMode (BUTTON2, INPUT_PULLUP);
  pinMode (BUZZER, OUTPUT);
  pinMode (TEMP, INPUT);
  pinMode (PIN_POTMETER, INPUT);
  LastKnobState= analogRead(PIN_POTMETER);
  Serial.begin(9600);
}

////take the time to show if nessar
//String GetTime(String laptime)
//{
//  if (laptime.length() == 5)
//  {
//    return laptime;
//  }
//  return "";
//}

//setting the alarm. 
void Alarm(unsigned long currentTime)
{
  if(currentTime - prevTime > 2*Interval)
  {
    tone(BUZZER, TONE_C4, QUARTERNOTE);
    digitalWrite(RED_LIGHT, HIGH);
    digitalWrite(RED_LIGHT, LOW); 
  }
}


//check the temperature. 
float get_temperature()                   //Function to read the Temperature Sensor.
{
  float temperature, resistance;
  int TempValue;
  TempValue = analogRead(TEMP);
  resistance = (float)TempValue * TEMP_R25 / (1024 - TempValue); // Calculate resistance
  temperature = 1 / (log(resistance / TEMP_R25) / TEMP_MATERIAL_CONSTANT + 1 / 298.15) - 273.15;
  return temperature;
}

//get the knob of the angle
int get_knob_angle()
{
  int sensor_value = analogRead(PIN_POTMETER);
  int angle;
  // map is an Arduino library function.
  // it maps one range to another range.
  angle = map(sensor_value, 0, 1023, 0, MAX_ANGLE);
  currentTime = millis();
  prevTime = millis();
  return angle;
}
// loop 
void loop() {
  // put your main code here, to run repeatedly:
 
  digitalWrite(GREEN_LIGHT, HIGH);
  
  String line = Serial.readStringUntil('\n');
  String line1 = Serial.readStringUntil('\n');

  float humidity = DHT11.getHumidity();
  float temperature = DHT11.getTemperature();
  float a = light();

  float x = 0;
  float y = 0;

  if ( line == "Raise")
  {
    x = x + 100;
    x++;
  }
  if ( line1 == "Sprinker")
  {
    y = y + 100;
    y++;
  }

  Serial.print(temperature);
  Serial.print(",");
  Serial.print(humidity + y);
  Serial.print(",");
  Serial.print(a + x);
  Serial.println();

  if ((a + x) < topshades) {
    digitalWrite(YELLOW_LIGHT, LOW);
  }
  if ((a + x) > topshades) {
    digitalWrite(YELLOW_LIGHT, HIGH);
  }

  if((humidity + y) < minhumidity) {
//    digitalWrite(BLUE_LIGHT, LOW);  
  blinkblue() ;
  }
  if((humidity + y) > minhumidity) {
//    digitalWrite(BLUE_LIGHT, LOW);
   blinkblue();
  }
  
  int Temp = get_temperature();
  int Mode_reading = digitalRead(BUTTON1);
  
  float celcius;                                // To show room temperature all the time
    celcius = get_temperature();
//    Serial.println(celcius);
 //setting up
  if ((millis() - Mode_lastDebounceTime) > debounceDelay)     //Debounce the Mode Button
  {
    if (Mode_reading != Mode_buttonState)
    {
      Mode_buttonState = Mode_reading;

      if (Mode_buttonState == LOW)        //To change mode with 1 button.
      {
        if (Mode == 0)
        {
          Mode = 1;
        }
        else if (Mode == 1)
        {
          Mode = 2;
          
        }
        else if (Mode == 2)
        {
          Mode = 3;
        }
        else if (Mode == 3)
        {
          Mode = 0;
        }
      }
    }
  }
   //this one to close the application when i click to the button of the C# to close. 
    if (line == "Stop_the_application")
  {
    digitalWrite(RED_LIGHT, HIGH);
    digitalWrite(GREEN_LIGHT, LOW);
    Display.show("-0FF");
    for (;;);
  }
 
  Mode_lastButtonState = Mode_reading;
//  String Time = GetTime(line);
  currentTime = millis();

  if (Mode == 0)
  {
    Display.show(a + x);
  //show light
  }
  if (Mode == 1)
  { 
  Display.show(humidity + x);
  }
  if (Mode == 2)
  {
    //show temperature
    float celcius;
    celcius = get_temperature();
    Display.show(celcius);
//    Serial.println(celcius);
//    digitalWrite(GREEN_LIGHT, HIGH); 
  }
  if (Mode == 3)
  {
    int angle;
    angle = get_knob_angle();
    Display.show(angle);
  }
  
  currentTime = millis();
  //set alarm for anytime
  int Alarm_reading = digitalRead(BUTTON2);
  if ((millis() - Alarm_lastDebounceTime) > debounceDelay)        //Debounce the Alarm Button
  {
    if (Alarm_reading != Alarm_buttonState)
    {
      Alarm_buttonState = Alarm_reading;
     if(Alarm_buttonState == LOW)
     {
//      Serial.println("ALARM ON");
      Alarm(currentTime);
     }
    }
  }
  Alarm_lastButtonState = Alarm_reading;

  // change to another mode imediatly. 
    int CurrentKnobState = analogRead(PIN_POTMETER);
    if(abs(CurrentKnobState - LastKnobState) > 20){
      Mode = 3;
      LastKnobState = CurrentKnobState;
    }
   
}
// set the value for light. 
float light()
{
  int sensorValue = analogRead(PIN_LDR);
  float resistance_sensor;
  // and convert to resistance in Kohms
  resistance_sensor = (float)(1023 - sensorValue) * 10 / sensorValue;

  float lux;
  lux = 325 * pow(resistance_sensor, -1.4);
  return lux;
}

int delayPeriod = 500;
//blink 5 time some time it's slow. 
void blinkblue() {
  static int i = 0;
  for (; i < 5; i++) {
    digitalWrite(BLUE_LIGHT, HIGH);
    delay(delayPeriod);
    digitalWrite(BLUE_LIGHT, LOW);
    delay(delayPeriod);
  }
}
//make the light blink five times.
