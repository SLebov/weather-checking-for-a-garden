﻿namespace Repair_Assigment
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.Shades = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbWindown = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbShades = new System.Windows.Forms.Label();
            this.btnWindown = new System.Windows.Forms.Button();
            this.btnRaise = new System.Windows.Forms.Button();
            this.btnCloseSprinkler = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lbSprinkler = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.listBoxWork = new System.Windows.Forms.ListBox();
            this.lbFire = new System.Windows.Forms.Label();
            this.lbBrightness = new System.Windows.Forms.Label();
            this.lbWet = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(32, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Windows";
            // 
            // Shades
            // 
            this.Shades.AutoSize = true;
            this.Shades.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Shades.Location = new System.Drawing.Point(34, 166);
            this.Shades.Name = "Shades";
            this.Shades.Size = new System.Drawing.Size(64, 20);
            this.Shades.TabIndex = 1;
            this.Shades.Text = "Shades";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(34, 223);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Sprinkler";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(34, 53);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(56, 20);
            this.lblStatus.TabIndex = 3;
            this.lblStatus.Text = "Status";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Lime;
            this.panel1.Controls.Add(this.lbWindown);
            this.panel1.Location = new System.Drawing.Point(121, 90);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(101, 39);
            this.panel1.TabIndex = 4;
            // 
            // lbWindown
            // 
            this.lbWindown.AutoSize = true;
            this.lbWindown.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbWindown.Location = new System.Drawing.Point(5, 8);
            this.lbWindown.Name = "lbWindown";
            this.lbWindown.Size = new System.Drawing.Size(48, 20);
            this.lbWindown.TabIndex = 2;
            this.lbWindown.Text = "Open";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Yellow;
            this.panel2.Controls.Add(this.lbShades);
            this.panel2.Location = new System.Drawing.Point(121, 154);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(101, 39);
            this.panel2.TabIndex = 5;
            // 
            // lbShades
            // 
            this.lbShades.AutoSize = true;
            this.lbShades.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbShades.Location = new System.Drawing.Point(5, 6);
            this.lbShades.Name = "lbShades";
            this.lbShades.Size = new System.Drawing.Size(70, 20);
            this.lbShades.TabIndex = 2;
            this.lbShades.Text = "Lowered";
            // 
            // btnWindown
            // 
            this.btnWindown.Location = new System.Drawing.Point(245, 90);
            this.btnWindown.Name = "btnWindown";
            this.btnWindown.Size = new System.Drawing.Size(103, 39);
            this.btnWindown.TabIndex = 6;
            this.btnWindown.Text = "Close Window";
            this.btnWindown.UseVisualStyleBackColor = true;
            this.btnWindown.Click += new System.EventHandler(this.btnWindown_Click);
            // 
            // btnRaise
            // 
            this.btnRaise.Location = new System.Drawing.Point(245, 154);
            this.btnRaise.Name = "btnRaise";
            this.btnRaise.Size = new System.Drawing.Size(103, 35);
            this.btnRaise.TabIndex = 7;
            this.btnRaise.Text = "Raise Shades ";
            this.btnRaise.UseVisualStyleBackColor = true;
            this.btnRaise.Click += new System.EventHandler(this.btnRaise_Click);
            // 
            // btnCloseSprinkler
            // 
            this.btnCloseSprinkler.Location = new System.Drawing.Point(245, 211);
            this.btnCloseSprinkler.Name = "btnCloseSprinkler";
            this.btnCloseSprinkler.Size = new System.Drawing.Size(103, 41);
            this.btnCloseSprinkler.TabIndex = 8;
            this.btnCloseSprinkler.Text = "Close Sprinkler";
            this.btnCloseSprinkler.UseVisualStyleBackColor = true;
            this.btnCloseSprinkler.Click += new System.EventHandler(this.btnCloseSprinkler_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Turquoise;
            this.panel3.Controls.Add(this.lbSprinkler);
            this.panel3.Location = new System.Drawing.Point(121, 211);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(101, 41);
            this.panel3.TabIndex = 9;
            // 
            // lbSprinkler
            // 
            this.lbSprinkler.AutoSize = true;
            this.lbSprinkler.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSprinkler.Location = new System.Drawing.Point(5, 9);
            this.lbSprinkler.Name = "lbSprinkler";
            this.lbSprinkler.Size = new System.Drawing.Size(41, 20);
            this.lbSprinkler.TabIndex = 0;
            this.lbSprinkler.Text = "OFF";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(35, 283);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 20);
            this.label6.TabIndex = 10;
            this.label6.Text = "Temperature:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(351, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 18);
            this.label7.TabIndex = 12;
            this.label7.Text = "Event logs";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(35, 320);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 20);
            this.label8.TabIndex = 13;
            this.label8.Text = "Brightness: ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(36, 362);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 20);
            this.label9.TabIndex = 14;
            this.label9.Text = "Humidity:";
            // 
            // listBoxWork
            // 
            this.listBoxWork.FormattingEnabled = true;
            this.listBoxWork.Items.AddRange(new object[] {
            "ALARM raised: Brightness above threshold ",
            "Shades manually closed "});
            this.listBoxWork.Location = new System.Drawing.Point(354, 79);
            this.listBoxWork.Name = "listBoxWork";
            this.listBoxWork.Size = new System.Drawing.Size(281, 303);
            this.listBoxWork.TabIndex = 15;
            // 
            // lbFire
            // 
            this.lbFire.AutoSize = true;
            this.lbFire.Location = new System.Drawing.Point(152, 288);
            this.lbFire.Name = "lbFire";
            this.lbFire.Size = new System.Drawing.Size(41, 13);
            this.lbFire.TabIndex = 16;
            this.lbFire.Text = "Celcius";
            // 
            // lbBrightness
            // 
            this.lbBrightness.AutoSize = true;
            this.lbBrightness.Location = new System.Drawing.Point(152, 327);
            this.lbBrightness.Name = "lbBrightness";
            this.lbBrightness.Size = new System.Drawing.Size(44, 13);
            this.lbBrightness.TabIndex = 17;
            this.lbBrightness.Text = "Number";
            // 
            // lbWet
            // 
            this.lbWet.AutoSize = true;
            this.lbWet.Location = new System.Drawing.Point(152, 369);
            this.lbWet.Name = "lbWet";
            this.lbWet.Size = new System.Drawing.Size(44, 13);
            this.lbWet.TabIndex = 18;
            this.lbWet.Text = "Percent";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // serialPort1
            // 
            this.serialPort1.PortName = "COM5";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.ClientSize = new System.Drawing.Size(665, 419);
            this.Controls.Add(this.lbWet);
            this.Controls.Add(this.lbBrightness);
            this.Controls.Add(this.lbFire);
            this.Controls.Add(this.listBoxWork);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.btnCloseSprinkler);
            this.Controls.Add(this.btnRaise);
            this.Controls.Add(this.btnWindown);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Shades);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Green House Control ";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Shades;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbWindown;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lbShades;
        private System.Windows.Forms.Button btnRaise;
        private System.Windows.Forms.Button btnCloseSprinkler;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lbSprinkler;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ListBox listBoxWork;
        private System.Windows.Forms.Label lbFire;
        private System.Windows.Forms.Label lbBrightness;
        private System.Windows.Forms.Timer timer1;
        private System.IO.Ports.SerialPort serialPort1;
        internal System.Windows.Forms.Label lbWet;
        public System.Windows.Forms.Button btnWindown;
    }
}

