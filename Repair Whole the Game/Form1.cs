﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Repair_Assigment
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            serialPort1.Open();     
            timer1.Start();
   
            timer1.Tick += timer1_Tick;
            timer1.Interval = 200;
            timer1.Enabled = true;
            string data_received = serialPort1.ReadLine();   
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (serialPort1.BytesToRead > 0 && serialPort1.IsOpen)
            {
                String dataFromArduino = serialPort1.ReadLine().ToString();
                String[] dataTempHumid = dataFromArduino.Split(',');
                //recieve data from arduino, comma mean read the number after each comma. 
                
               int Temperature = (int)(Math.Round(Convert.ToDecimal(dataTempHumid[0]), 0));
               int Humidity = (int)(Math.Round(Convert.ToDecimal(dataTempHumid[1]), 0));
               int Bright = (int)(Math.Round(Convert.ToDecimal(dataTempHumid[2]), 0));
                //read the the number arduino and receive it. 
                lbFire.Text = Temperature.ToString() + "°C";
                lbWet.Text = Humidity.ToString() + "%";
                lbBrightness.Text = Bright.ToString();
                //show it in oC, %, light. 
                if(Temperature < 15)
                {
                    lbWindown.Text = "Close";
                    panel1.BackColor = Color.Red;
                    serialPort1.WriteLine("Stop_the_windown");
                }
                //if Temperature is too small it's will close.
                else
                {
                    lbWindown.Text = "ON";
                    panel1.BackColor = Color.Lime;              
                }
                //if the temperature is too small the arduino may close according to the requirement. 

                if (Humidity < 96)
                {
                    lbSprinkler.Text = "ON";
                    panel3.BackColor = Color.LightSalmon;
                }
                else if (Humidity > 96)
                {
                    lbSprinkler.Text = "OFF";
                    panel3.BackColor = Color.Turquoise;
                    listBoxWork.Items.Add("ALARM raised: Humidity is hight, sprinkler is off");
                }
                //if temperautre is above 96 of humidity ratio. Consquetly, Sprikler is On. Otherwise, Sprinkler is OFF. 
                
                if (Bright > 100)
                {
                    panel2.BackColor = Color.Yellow;
                    lbShades.Text = "LOWERED";
                    listBoxWork.Items.Add("ALARM raised: Brightness above threshold Shades manually closed ");
                }
                else if (Bright < 100)
                {
                    panel2.BackColor = Color.DeepSkyBlue;
                    lbShades.Text = "NORMAL";
                }                                      
            }
            
        }
       
        private void btnWindown_Click(object sender, EventArgs e)
        {
            serialPort1.WriteLine("Stop_the_application");

            lbWindown.Text = "Close";
            panel1.BackColor = Color.Red;
            listBoxWork.Items.Add("The arduino is turn off, all light, temperature, humidity is off");
            //send to the aruduino and it's will show OFF.
            //if you click this, everything will off. 
        }
        private void btnCloseSprinkler_Click(object sender, EventArgs e)
        {
            serialPort1.WriteLine("Sprinker");
            //send the message to close Sprinker.
        }
        private void btnRaise_Click(object sender, EventArgs e)
        {
            serialPort1.WriteLine("Raise");      
            //read the shades. 
        }

        
    }
}
